import os
import json
import datetime
import pytz
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Application, CommandHandler, CallbackQueryHandler, CallbackContext

from finamrest import FinamRest


class MyTelegramBot:
    def __init__(self, token=None):
        if token is None:
            current_directory = os.path.dirname(os.path.abspath(__file__))
            file_path = os.path.join(current_directory, 'my_data.json')
            with open(file_path, 'r') as file:
                data = json.load(file)
                access_token = data['token']
        print(access_token)
        self.token = access_token
        self.application = Application.builder().token(self.token).build()

    async def start(self, update: Update, context: CallbackContext) -> None:
        keyboard = [
            [InlineKeyboardButton("Предоставить информацию", callback_data='provide_info')],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text('Привет! Нажми на кнопку ниже.', reply_markup=reply_markup)

    async def button(self, update: Update, context: CallbackContext) -> None:
        query = update.callback_query
        await query.answer()
        if query.data == 'provide_info':
            finam = FinamRest()

            # Получаем текущее время UTC
            now_utc = datetime.datetime.now(pytz.utc)

            # Отформатировать время в виде "yyyy-MM-ddTHH:mm:ssZ"
            formatted_time = now_utc.strftime('%Y-%m-%dT%H:%M:%SZ')

            data = finam.get_intraday_candles('TQBR', 'SBER', 'M1', date_to=formatted_time, count=1)

            result = []
            for item in data['candles']:
                timeframe = item['timestamp']
                close = item['close']['num'] * 10 ** (-item['close']['scale'])
                result.append({'timeframe': timeframe, 'close': close})

            await query.edit_message_text(text=f"Информация по SBER: {result[-1]['timeframe']}, {result[-1]['close']}")

    def run(self):
        # Настройка обработчика команд и кнопок
        self.application.add_handler(CommandHandler('start', self.start))
        self.application.add_handler(CallbackQueryHandler(self.button))

        # Начало опроса и запуск бота
        self.application.run_polling()
