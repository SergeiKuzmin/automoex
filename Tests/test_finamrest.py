import json
import datetime
import pytz
from finamrest import FinamRest


finam = FinamRest()

# print(finam.check_access_token())

# Получаем текущее время UTC
now_utc = datetime.datetime.now(pytz.utc)

# Отформатировать время в виде "yyyy-MM-ddTHH:mm:ssZ"
formatted_time = now_utc.strftime('%Y-%m-%dT%H:%M:%SZ')

print(now_utc)
print(formatted_time)

data = finam.get_intraday_candles('TQBR', 'SBER', 'M1', date_to=formatted_time, count=2)

print(data)
print(data.get('candles'))

result = []
for item in data['candles']:
    timeframe = item['timestamp']
    close = item['close']['num'] * 10 ** (-item['close']['scale'])
    result.append({'timeframe': timeframe, 'close': close})

print(result)
