import os
import json
from requests import post, get, delete  # Запросы/ответы от сервера запросов
from json import loads  # Ответы принимаются в виде JSON сообщений


class FinamRest(object):
    def __init__(self, client_id=None, access_token=None):
        """
            Инициализация

            :param str client_id: Идентификатор торгового счёта
            :param str access_token: Торговый токен доступа
        """
        if client_id is None or access_token is None:
            # Чтение значения токена из файла
            # Получаем путь к текущему каталогу
            current_directory = os.path.dirname(os.path.abspath(__file__))
            file_path = os.path.join(current_directory, 'my_data.json')
            with open(file_path, 'r') as file:
                data = json.load(file)
                client_id = data['client_id']
                access_token = data['token']
                print(client_id, access_token)

        self.server = 'https://trade-api.finam.ru'
        self.client_id = client_id  # Идентификатор торгового счёта
        self.access_token = access_token  # Торговый токен доступа

    @staticmethod
    def check_result(response):
        """
            Анализ результата запроса

            :param response response: Результат запроса
            :return: Справочник из JSON, текст, None в случае веб ошибки
        """
        # if response.status_code != 200:  # Если статус ошибки
        #     self.OnError(f'Ошибка сервера: {response.status_code} {response.content.decode("utf-8")} {response.request}')  # Событие ошибки
        #     return 'Server error'  # то возвращаем пустое значение
        content = loads(response.content.decode('utf-8'))  # Декодируем полученное значение JSON в справочник
        error = content['error']  # Данные об ошибке
        return content['data']  # Возвращаем полученное значение

    def get_headers(self):
        """
            Получение хедеров для запросов
        """
        return {'accept': 'text/plain', 'X-Api-Key': self.access_token}

    def check_access_token(self):
        """
            Проверка токена
        """
        return self.check_result(get(url=f'{self.server}/api/v1/access-tokens/check', headers=self.get_headers()))

    def get_portfolio(self, include_currencies=True, include_money=True, include_positions=True, include_max_buy_sell=True):
        """
            Возвращает портфель

            :param bool include_currencies: Валютные позиции
            :param bool include_money: Денежные позиции
            :param bool include_positions: Позиции DEPO
            :param bool include_max_buy_sell: Лимиты покупки и продажи
        """
        params = {'ClientId': self.client_id,
                  'Content.IncludeCurrencies': include_currencies,
                  'Content.IncludeMoney': include_money,
                  'Content.IncludePositions': include_positions,
                  'Content.IncludeMaxBuySell': include_max_buy_sell}

        return self.check_result(get(url=f'{self.server}/api/v1/portfolio', params=params, headers=self.get_headers()))

    def get_intraday_candles(self, security_board, security_code, time_frame, date_from=None, date_to=None, count=None):
        """
            Запрос внутридневных свечей

            - Максимальный интервал: 30 дней
            - Максимальное кол-во запросов в минуту: 120

            :param str security_board: Режим торгов
            :param str security_code: Тикер инструмента
            :param str time_frame: Временной интервал внутридневной свечи 'M1' - 1 минута, 'M5' - 5 минут, 'M15' - 15 минут, 'H1' - 1 час
            :param str date_from: Дата начала в формате yyyy-MM-ddTHH:mm:ssZ в часовом поясе UTC
            :param str date_to: Дата окончания в формате yyyy-MM-ddTHH:mm:ssZ в часовом поясе UTC
            :param int count: Кол-во свечей. Максимум 500
        """
        params = {'SecurityBoard': security_board,
                  'SecurityCode': security_code,
                  'TimeFrame': time_frame,
                  'Interval.From': date_from,
                  'Interval.To': date_to,
                  'Interval.Count': count}

        # if date_from:  # Если указана дата начала
        #     params['from'] = date_from  # то выставляем ее
        # if date_to:  # Если указана дата окончания
        #     params['to'] = date_from  # то выставляем ее
        # if count:  # Если указано кол-во свечей
        #     params['count'] = count  # то выставляем их
        return self.check_result(get(url=f'{self.server}/api/v1/intraday-candles', params=params,
                                     headers=self.get_headers()))
